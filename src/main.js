import { createApp } from 'vue'
import App from './App.vue'

import PrimeVue from '../node_modules/primevue/config'
import Button from '../node_modules/primevue/button'
import Dropdown from 'primevue/dropdown'

const app = createApp(App)

app.use(PrimeVue)

app.component('Button', Button)
app.component('Dropdown', Dropdown)

app.mount('#app')
